import { EntityStore, StoreConfig } from '@datorama/akita';
import { ProfileState } from './profile.state';
import { Injectable } from '@angular/core';

@StoreConfig({
  name: 'profile'
})
@Injectable({
  providedIn: 'root'
})
export class ProfileStore extends EntityStore<ProfileState> {
  constructor() {
    super();
  }
}
