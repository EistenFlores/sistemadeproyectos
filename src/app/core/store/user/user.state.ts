import { EntityState } from "@datorama/akita";
import { User } from "src/app/models/user.model";

export interface UserState extends EntityState<User> {}
