import { SisproApi } from "../base/sispro.api";
import { Injectable } from "@angular/core";
import { User } from "src/app/models/user.model";

@Injectable({
  providedIn: "root"
})
export class AuthApi extends SisproApi {
  login(identifier: string, password: string) {
    return this.api.post(
      "/auth/local",
      {
        identifier: identifier.toLowerCase(),
        password
      },
      {
        isAuthenticated: false
      }
    );
  }
  register(userReq: User) {
    return this.api.post(
      "/auth/local/register",
      {
        username: userReq.correo.toLowerCase().replace("@", "__at__"),
        email: userReq.correo.toLowerCase(),
        password: userReq.contrasena
      },
      {
        isAuthenticated: false
      }
    );
  }
}
