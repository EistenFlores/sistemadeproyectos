import { BaseModel } from "./base/base.model";

export class File extends BaseModel<File> {
  file: Blob;
  url: string;
}
