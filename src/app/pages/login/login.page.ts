import { Component, OnInit } from "@angular/core";
import { Facebook, FacebookLoginResponse } from "@ionic-native/facebook/ngx";
import { GooglePlus } from "@ionic-native/google-plus/ngx";
import { FormBuilder, Validators } from "@angular/forms";
import { User } from "../../models/user.model";

import { UserService } from "../../core/services/user.service";
import { AuthService } from "../../core/services/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  isLoggedIn = false;
  users = {
    id: "",
    first_name: "",
    last_name: "",
    celular: "",
    contrasena: "",
    email: "",
    picture: { data: { url: "" } }
  };

  constructor(
    private fb: Facebook,
    private googlePlus: GooglePlus,
    private userService: UserService,
    private authService: AuthService,
    private formBuilder: FormBuilder
  ) {
    fb.getLoginStatus()
      .then(res => {
        console.log(res.status);
        if (res.status === "connect") {
          this.isLoggedIn = true;
        } else {
          this.isLoggedIn = false;
        }
      })
      .catch(e => console.log(e));
  }

  checkoutForm = this.formBuilder.group({
    correo: ["", [Validators.required, Validators.pattern(/.+@.+\..+/)]],
    contrasena: ["", [Validators.required]]
  });

  ingresar() {
    const user = this.checkoutForm.value;
    this.authService.login(user.correo, user.contrasena);
  }

  ngOnInit() {}

  loginFB() {
    this.fb
      .login(["public_profile" /*'user_friends'*/, "email"])
      .then(res => {
        if (res.status === "connected") {
          this.isLoggedIn = true;
          this.getUserDetailFB(res.authResponse.userID);
        } else {
          this.isLoggedIn = false;
        }
      })
      .catch(e => console.log("Error logging into Facebook", e));
  }

  getUserDetailFB(userid: any) {
    this.fb
      .api("/" + userid + "/?fields=id,email,first_name,last_name,picture", [
        "public_profile"
      ])
      .then(res => {
        console.log(res);
        this.users = res;
        this.users.contrasena = res.id;
        console.log(this.users);

        // this.authService.register(
        //   {
        //     correo: this.users.email,
        //     contrasena: this.users.contrasena
        //   },
        //   {
        //     nombre: this.users.first_name,
        //     apellidos: this.users.last_name,
        //     celular: "45512"
        //   }
        // );
      })
      .catch(e => {
        console.log(e);
      });
  }

  logout() {
    this.fb
      .logout()
      .then(res => (this.isLoggedIn = false))
      .catch(e => console.log("Error logout from Facebook", e));
    this.googlePlus
      .logout()
      .then(res => (this.isLoggedIn = false))
      .catch(e => console.log("Error logout from Google", e));
  }

  loginGoogle() {
    this.googlePlus
      .login({})
      .then(res => {
        console.log(res.userId + res.givenName + res.familyName + res.email);
        this.users = {
          celular: "",
          id: res.userId,
          contrasena: res.userId,
          first_name: res.givenName,
          last_name: res.familyName,
          email: res.email,
          picture: { data: { url: "../../assets/img/user-icon-vector.jpg" } }
        };
        console.log(this.users);
        // this.authService.register(
        //   {
        //     correo: this.users.email,
        //     contrasena: this.users.contrasena
        //   },
        //   {
        //     nombre: this.users.first_name,
        //     apellidos: this.users.last_name,
        //     celular: "5413124"
        //   }
        // );
        this.isLoggedIn = true;
      })
      .catch(err => console.error(err));
  }
}
