import { Component, OnInit } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { Camera } from "@ionic-native/camera/ngx";
import { AuthService } from "../../core/services/auth.service";
import { User } from "../../models/user.model";
import { Profile } from "src/app/models/profile.model";
import { b64toBlob } from "../../lib/helpers/file.helper";

@Component({
  selector: "app-register",
  templateUrl: "./register.page.html",
  styleUrls: ["./register.page.scss"]
})
export class RegisterPage implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private camera: Camera,
    private authService: AuthService
  ) {}
  imagen;
  isCamaraIn = "";
  // isRegisterIn = false

  users = {
    nombre: "",
    apellidos: "",
    contrasena: "",
    correo: "",
    // foto_user: { data: { url: "" } },
    celular: ""
  };

  checkoutForm = this.formBuilder.group({
    nombre: ["", [Validators.required]],
    apellidos: ["", [Validators.required]],
    correo: ["", [Validators.required, Validators.pattern(/.+@.+\..+/)]],
    contrasena: ["", [Validators.required]],
    // foto_user: {
    //   data: {
    //     url: "data:image/jpeg;base64,../../assets/img/user-icon-vector.jpg"
    //   }
    // },
    celular: ["", [Validators.required]]
  });

  ngOnInit() {}

  onSubmit() {
    console.log(this.checkoutForm.value);
    this.users = this.checkoutForm.value;
    console.log(this.users);
    this.authService.register(
      {
        correo: this.users.correo,
        contrasena: this.users.contrasena
      },
      {
        nombre: this.users.nombre,
        apellidos: this.users.apellidos,
        celular: this.users.celular,
        // foto_user: {
        //   file: b64toBlob(this.users.foto_user.data.url, "image/jpeg"),
        //   url: this.users.foto_user.data.url
        // }
      }
    );
    // this.isRegisterIn = true
  }

  subirFoto() {
    this.camera
      .getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        mediaType: this.camera.MediaType.PICTURE,
        allowEdit: false,
        encodingType: this.camera.EncodingType.JPEG,
        targetHeight: 300,
        targetWidth: 300,
        correctOrientation: true,
        saveToPhotoAlbum: true
      })
      .then(resultado => {
        this.imagen = "data:image/jpeg;base64," + resultado;
        this.checkoutForm.patchValue({
          foto_user: { data: { url: this.imagen } }
        });
        console.log(this.imagen);
      })
      .catch(error => {
        console.log(error);
      });
  }
  tomarFoto() {
    this.camera
      .getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType: this.camera.PictureSourceType.CAMERA,
        mediaType: this.camera.MediaType.PICTURE,
        allowEdit: false,
        encodingType: this.camera.EncodingType.JPEG,
        targetHeight: 300,
        targetWidth: 300,
        correctOrientation: true,
        saveToPhotoAlbum: true
      })
      .then(resultado => {
        this.imagen = "data:image/jpeg;base64," + resultado;
        this.imagen = resultado;
        this.checkoutForm.patchValue({
          foto_user: { data: { url: this.imagen } }
        });
        console.log(this.imagen);
      })
      .catch(error => {
        console.log(error);
      });
  }

  onSelect(data: string) {
    this.isCamaraIn = data;
  }
  logout() {
    // this.isRegisterIn = false
  }
}
