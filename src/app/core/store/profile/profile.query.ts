import { QueryEntity } from "@datorama/akita";
import { ProfileState } from "./profile.state";
import { Injectable } from "@angular/core";
import { ProfileStore } from "./profile.store";

@Injectable({
  providedIn: "root"
})
export class ProfilesQuery extends QueryEntity<ProfileState> {
  Profiles$ = this.selectAll();
  getProfiles() {
    this.getAll();
  }

  constructor(store: ProfileStore) {
    super(store);
  }
}
