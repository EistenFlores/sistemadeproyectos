import { EntityState } from '@datorama/akita';
import { Profile } from 'src/app/models/profile.model';

export interface ProfileState extends EntityState<Profile> {}
