import { Component, OnInit } from "@angular/core";
import { UserService } from "src/app/core/services/user.service";
import { UserQuery } from "src/app/core/store/user/user.query";
import { SessionQuery } from "src/app/core/store/session/session.query";
import { ProfilesQuery } from "src/app/core/store/profile/profile.query";
import { ProfileService } from "src/app/core/services/profile.service";
import { AuthService } from "src/app/core/services/auth.service";

@Component({
  selector: "app-perfil",
  templateUrl: "./perfil.page.html",
  styleUrls: ["./perfil.page.scss"]
})
export class PerfilPage implements OnInit {
  constructor(
    private userService: UserService,
    private profileService: ProfileService,
    public userQuery: UserQuery,
    public sessionQuery: SessionQuery,
    public profileQuery: ProfilesQuery,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.userService.getUser();
    this.profileService.getProfile(this.sessionQuery.getLoggedUserId());
  }

  logout() {
    this.authService.logout();
  }
}
