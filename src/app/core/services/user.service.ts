import { Injectable } from "@angular/core";
import { UserStore } from "../store/user/user.store";
import { UserApi } from "../api/proyect/user.api";
import { tap } from "rxjs/operators";

@Injectable({ providedIn: "root" })
export class UserService {
  constructor(private userStore: UserStore, private userApi: UserApi) {}

  getUser() {
    this.userApi
      .getUser()
      .pipe(
        tap(user => {
          this.userStore.set({
            _id: user._id,
            correo: user.email
          });
          console.log(this.userStore["storeValue"]["entities"]);
        })
      )
      .subscribe();
  }
}
