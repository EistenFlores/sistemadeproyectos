import { Injectable } from "@angular/core";
import { UserApi } from "../api/proyect/user.api";
import { SessionStore } from "../store/session/session.store";

@Injectable({
  providedIn: "root"
})
export class SessionService {
  constructor(private userApi: UserApi, private sessionStore: SessionStore) {}
}
