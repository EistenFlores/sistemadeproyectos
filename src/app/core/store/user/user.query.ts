import { QueryEntity } from "@datorama/akita";
import { UserState } from "./user.state";
import { Injectable } from "@angular/core";
import { UserStore } from "./user.store";
import { UserApi } from "../../api/proyect/user.api";
import { tap, switchMap } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class UserQuery extends QueryEntity<UserState> {
  constructor(private userStore: UserStore, private userApi: UserApi) {
    super(userStore);
  }
  // tengo el usuario
  user$ = this.selectAll();

}
