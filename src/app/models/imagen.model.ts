import { BaseModel } from "./base/base.model";
import { File } from "./file.model";

export class Image extends BaseModel<Image> {
  name: string;
  code: string;
  file: File;
  type: string;
}
