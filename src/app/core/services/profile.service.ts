import { Injectable } from "@angular/core";
import { ProfileApi } from "../api/proyect/profile.api";
import { SessionQuery } from "../store/session/session.query";
import { tap, switchMap } from "rxjs/operators";
import { ProfileStore } from "../store/profile/profile.store";
import { ID } from "@datorama/akita";

@Injectable({
  providedIn: "root"
})
export class ProfileService {
  constructor(
    private profileApi: ProfileApi,
    private profileStore: ProfileStore
  ) {}
  getProfile(idProfile: ID) {
    // console.log(idProfile);
    this.profileApi
      .getProfile(idProfile)
      .pipe(
        tap(profile => {
          this.profileStore.set({
            nombre: profile.nombre,
            apellidos: profile.apellidos,
            celular: profile.celular
          });
          console.log(this.profileStore["storeValue"]["entities"]);
        })
      )
      .subscribe();
  }
}
