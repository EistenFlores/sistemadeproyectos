import { EntityStore, StoreConfig } from '@datorama/akita';
import { UserState } from './user.state';
import { Injectable } from '@angular/core';

@StoreConfig({
  name: 'user'
})
@Injectable({
  providedIn: 'root'
})
export class UserStore extends EntityStore<UserState> {
  constructor() {
    super();
  }
}
