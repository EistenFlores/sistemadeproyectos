import { SisproApi } from "../base/sispro.api";
import { Injectable } from "@angular/core";
import { ID } from "@datorama/akita";
import { Profile } from "src/app/models/profile.model";
import { User } from "src/app/models/user.model";

@Injectable({
  providedIn: "root"
})
export class ProfileApi extends SisproApi {
  getProfile(idProfile: ID) {
    return this.api.get(`/perfils/${idProfile}`);
  }

  postProfile(userProfile: Profile, user: User) {
    return this.api.post("/perfils", {
      _id: user._id,
      nombre: userProfile.nombre,
      apellidos: userProfile.apellidos,
      celular: userProfile.celular,
      user: user._id
    });
  }
}
