import { Injectable } from "@angular/core";
import { ApiUtil } from "../../utils/api.util";

@Injectable({
  providedIn: "root"
})
export class SisproApi {
  constructor(protected api: ApiUtil) {}
}
