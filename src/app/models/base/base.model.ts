import { PartialModel } from "./partial.model";

export class BaseModel<T> extends PartialModel<T> {
  id?: any;
}
