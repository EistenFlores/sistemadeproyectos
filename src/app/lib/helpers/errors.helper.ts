export const getErrorMessageFromBeError = beError => {
    const error = beError;
    let errorMessage = null;
    if (
      error &&
      error.message &&
      error.message instanceof Array &&
      error.message[0]
    ) {
      errorMessage = error.message[0].messages[0].id;
    }
    return errorMessage;
  };
  