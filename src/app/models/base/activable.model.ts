import { BaseModel } from "./base.model";

export class ActivableModel<T> extends BaseModel<T> {
  active: boolean;
}
