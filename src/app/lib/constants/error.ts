export const EMAIL_TAKEN_ERROR = "Auth.form.error.email.taken";
export const INVALID_CREDENTIALS_ERROR = "Auth.form.error.invalid";
