import { BaseModel } from "./base/base.model";
import { File } from "./file.model";

export interface Profile extends BaseModel<Profile> {
  nombre?: string;
  apellidos?: string;
  foto_user?: File;
  celular?: string;
}

export function createProfile(params: Partial<Profile>) {
  return {} as Profile;
}
