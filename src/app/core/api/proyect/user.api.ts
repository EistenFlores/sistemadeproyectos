import { SisproApi } from "../base/sispro.api";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class UserApi extends SisproApi {
  getUser() {
    return this.api.get(`/users/me`);
  }
}
