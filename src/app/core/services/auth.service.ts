import { Injectable } from "@angular/core";
import { User } from "src/app/models/user.model";
import { ApiUtil } from "../utils/api.util";
import { tap, catchError, switchMap } from "rxjs/operators";
import { Profile } from "src/app/models/profile.model";
import { getErrorMessageFromBeError } from "src/app/lib/helpers/errors.helper";
import {
  INVALID_CREDENTIALS_ERROR,
  EMAIL_TAKEN_ERROR
} from "src/app/lib/constants/error";
import { AlertController, NavController } from "@ionic/angular";
import { SessionStore } from "../store/session/session.store";
import { AuthApi } from "../api/proyect/auth.api";
import { UserStore } from "../store/user/user.store";
import { ProfileApi } from "../api/proyect/profile.api";
import { ProfileStore } from "../store/profile/profile.store";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  constructor(
    private api: ApiUtil,
    private alertCtrl: AlertController,
    private navCtrl: NavController,
    private authApi: AuthApi,
    private sessionStore: SessionStore,
    private usersStore: UserStore,
    private profilesStore: ProfileStore,
    private profileApi: ProfileApi
  ) {}

  login(identifier: string, password: string) {
    console.log(identifier);
    console.log(password);
    this.authApi
      .login(identifier, password)
      .pipe(
        tap(loginResponse => {
          const { user, jwt } = loginResponse;
          localStorage.setItem("token", jwt);
          this.sessionStore.update({
            loggedUserId: user.id
          });
          this.usersStore.update(user.id, user);
        }),
        switchMap(loginResponse => {
          const { user } = loginResponse;
          return this.profileApi.getProfile(user._id).pipe(
            tap(profileResponse => {
              this.profilesStore.update(profileResponse.id, profileResponse);
            })
          );
        }),
        tap(() => {
          this.navCtrl.navigateRoot("/app", {
            animated: true,
            animationDirection: "forward"
          });
        }),
        catchError(async err => {
          const errorMessage = getErrorMessageFromBeError(err.error);
          if (errorMessage === INVALID_CREDENTIALS_ERROR) {
            const alertIns = await this.alertCtrl.create({
              header: "Credenciales incorrectas",
              message: "El correo o contraseña ingresados no son válidos",
              buttons: ["OK"]
            });
            alertIns.present();
          }
        })
      )
      .subscribe();
  }

  register(userReq: User, userProfile: Profile) {
    // console.log(userReq);
    this.authApi
      .register(userReq)
      .pipe(
        tap(registerResponse => {
          const { user, jwt } = registerResponse;
          localStorage.setItem("token", jwt);
          this.sessionStore.update({
            loggedUserId: user.id
          });
          this.usersStore.update(user.id, user);
        }),
        switchMap(registerResponse => {
          const { user } = registerResponse;
          return this.profileApi.postProfile(userProfile, user).pipe(
            tap(profileResponse => {
              this.profilesStore.update(profileResponse.id, profileResponse);
            })
          );
        }),
        tap(() => {
          this.navCtrl.navigateRoot("/app", {
            animated: true,
            animationDirection: "forward"
          });
        }),
        catchError(async err => {
          const errorMessage = getErrorMessageFromBeError(err.error);
          if (errorMessage === EMAIL_TAKEN_ERROR) {
            const alertIns = await this.alertCtrl.create({
              header: "Correo en uso",
              message: "El correo ya esta en uso",
              buttons: ["OK"]
            });
            alertIns.present();
          }
        })
      )
      .subscribe();
  }

  logout() {
    localStorage.removeItem("token");
    this.navCtrl.navigateRoot("/login", {
      animated: true,
      animationDirection: "forward"
    });
  }

  // subirFoto(photo: File, profileId: string) {
  //   const formData = new FormData();
  //   formData.append("files", photo, photo.name);
  //   formData.append("path", "foto_user");
  //   formData.append("refId", profileId);
  //   formData.append("ref", "perfil");
  //   formData.append("field", "foto");
  //   return this.api.post("/upload", formData);
  // }
}
